import './App.css';
import MyNavbar from './navbar/MyNavbar';
import Home from './pages/home/Home';
import About from './pages/About';
import Treatments from './pages/Treatments';
import Contact from './pages/Contact';
import Footer from './footer/Footer';

import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

require('dotenv').config();

function App() {
	return (
		<Router basename={process.env.PUBLIC_URL}>
			<div className='App wrapper' style={AppStyle}>
				<MyNavbar></MyNavbar>
				<Switch>
					<Route exact path='/' component={Home} />
					<Route exact path='/om-mig' component={About} />
					<Route exact path='/behandlingar' component={Treatments} />
					<Route exact path='/kontakt' component={Contact} />
				</Switch>
				<Footer></Footer>
			</div>
		</Router>
	);
}

const AppStyle = {
	display: 'flex',
	flexDirection: 'column',
	width: '100%',
	backgroundColor: '#EEEEEE',
	overflow: 'hidden',
};

export default App;
