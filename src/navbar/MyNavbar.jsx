import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Image from 'react-bootstrap/Image';

import { Link } from 'react-router-dom';
import Logo from '../resources/pictures/asasfot-v3.png';

export default function MyNavbar() {
	return (
		<div>
			<Navbar
				collapseOnSelect
				expand='lg'
				bg='light'
				variant='light'
				style={NavStyle}>
				<Navbar.Brand as={Link} to='/' style={BrandStyle}>
					<Image src={Logo} style={LogoStyle}></Image>
				</Navbar.Brand>
				<Navbar.Toggle aria-controls='responsive-navbar-nav' />
				<Navbar.Collapse id='responsive-navbar-nav'>
					<Nav className='mr-auto'></Nav>
					<Nav>
						<Nav.Link as={Link} to={'/'}>
							HEM
						</Nav.Link>
						<Nav.Link as={Link} to={'/om-mig'}>
							OM MIG
						</Nav.Link>
						<Nav.Link as={Link} to={'/behandlingar'}>
							MINA BEHANDLINGAR
						</Nav.Link>
						<Nav.Link as={Link} to={'/kontakt'}>
							KONTAKTA MIG
						</Nav.Link>
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		</div>
	);
}

const NavStyle = {
	marginTop: '0px',
	marginBottom: '0px',
	paddingTop: '0px',
	paddingBottom: '0px',
};

const BrandStyle = {
	height: '80px',
	width: '260px',
	marginLeft: '0px',
	marginRight: '0px',
	PaddingLeft: '0px',
	PaddingRight: '0px',
};

const LogoStyle = {
	height: '100%',
	width: '100%',
	borderRadius: '0%',
	display: 'flex',
	flexDirection: 'row',
	alignItems: 'flex-start',
	marginLeft: '0px',
	marginRight: '0px',
	PaddingLeft: '0px',
	PaddingRight: '0px',
};
