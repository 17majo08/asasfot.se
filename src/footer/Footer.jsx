import React from 'react';
import Image from 'react-bootstrap/Image';

import BusinessCard from '../resources/pictures/kontakta.jpg';

export default function Footer() {
	return (
		<div style={FlexContainer}>
			<div style={FlexItem} className='d-none d-lg-block'>
				<div>
					<Image src={BusinessCard} style={ImageStyle}></Image>
				</div>
			</div>
			<div style={FlexItem}>
				<h5>Kontaktinformation</h5>
				<div>Södra Hamngatan 8</div>
				<div>826 50 Söderhamn</div>
				<div>Telefon: 070 205 56 11</div>
			</div>
			<div style={FlexItem}>
				<h5>Öppettider</h5>
				<div>Mån-Fre: Tidsbokning 070 205 56 11</div>
				<div>Lörd-Sönd: Stängt</div>
			</div>
			<div style={FlexItem} className='d-block d-lg-none'>
				<div>
					<Image src={BusinessCard} style={ImageStyle}></Image>
				</div>
			</div>
		</div>
	);
}

const ImageStyle = {
	maxHeight: '150px',
};

const FlexContainer = {
	backgroundColor: '#dddddd',
	paddingTop: '10px',
	paddingBottom: '20px',
	paddingLeft: '20px',
	paddingRight: '20px',
	width: '100%',
	display: 'flex',
	flexDirection: 'row',
	flexWrap: 'wrap',
	justifyContent: 'center',
};

const FlexItem = {
	marginLeft: '20px',
	marginRight: '20px',
	marginTop: '20px',
	marginBottom: '20px',
};
