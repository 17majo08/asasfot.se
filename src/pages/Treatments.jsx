import React from 'react';

import TreatmentsSection from './home/sections/treatments/Treatments';

export default function Treatments() {
	return (
		<div>
			<TreatmentsSection></TreatmentsSection>
		</div>
	);
}
