import React, { useState } from 'react';
import ReactDOM from 'react-dom';

import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { Alert } from 'reactstrap';
import emailjs from 'emailjs-com';
import { init } from 'emailjs-com';
init(process.env.REACT_APP_USER_ID);

export default function Contact() {
	function sendEmail(e) {
		e.preventDefault();

		emailjs
			.sendForm(
				process.env.REACT_APP_SERVICE_ID,
				process.env.REACT_APP_TEMPLATE_ID,
				e.target,
				process.env.REACT_APP_USER_ID
			)
			.then(
				(result) => {
					ReactDOM.findDOMNode(messageForm).reset();
					setErrorVisible(false);
					setSuccessVisible(true);
				},
				(error) => {
					setSuccessVisible(false);
					setErrorVisible(true);
				}
			);
	}

	const [messageForm, setMessageForm] = useState(null);

	const [successVisible, setSuccessVisible] = useState(false);
	const onSuccessDismiss = () => setSuccessVisible(false);

	const [errorVisible, setErrorVisible] = useState(false);
	const onErrorDismiss = () => setErrorVisible(false);

	return (
		<div style={BaseContainerStyle}>
			<h3 className='Capitalized' style={HeaderStyle}>
				Kontakta mig
			</h3>
			<div style={ContainerStyle}>
				<Form
					style={FormStyle}
					ref={(form) => setMessageForm(form)}
					onSubmit={sendEmail}>
					<Form.Group controlId='user_name'>
						<Form.Label>Namn</Form.Label>
						<Form.Control name='user_name' placeholder='Namn' />
					</Form.Group>

					<Form.Group controlId='user_email'>
						<Form.Label>E-post</Form.Label>
						<Form.Control
							type='email'
							name='user_email'
							placeholder='Ange e-post'
						/>
					</Form.Group>

					<Form.Group controlId='message'>
						<Form.Label>Meddelande</Form.Label>
						<Form.Control
							as='textarea'
							name='message'
							placeholder='Meddelande'
							rows={10}
						/>
					</Form.Group>

					<Alert
						color='success'
						isOpen={successVisible}
						toggle={onSuccessDismiss}>
						Meddelandet har skickats!
					</Alert>
					<Alert color='danger' isOpen={errorVisible} toggle={onErrorDismiss}>
						Något gick fel. Försök igen!
					</Alert>

					<Button variant='primary' type='submit'>
						Skicka
					</Button>
				</Form>
			</div>
		</div>
	);
}

const ContainerStyle = {
	marginTop: '10px',
	alignSelf: 'center',
	maxWidth: '700px',
	width: '80%',
};

const BaseContainerStyle = {
	marginTop: '10px',
	display: 'flex',
	flexDirection: 'column',
	justifyContent: 'center',
	textAlign: 'center',
};

const FormStyle = {
	paddingBottom: '50px',
};

const HeaderStyle = {
	fontFamily: 'Benne',
	fontSize: '30px',
	marginTop: '40px',
};
