import React from 'react';
import Container from 'react-bootstrap/Container';

import TreatmentsCard from './TreatmentsCard';
import { CardColumns } from 'react-bootstrap';

import taktil from './../../../../resources/pictures/treatments/massage-helkropp.jpg';
import zonterapi from './../../../../resources/pictures/treatments/zonterapi.jpg';
import underben from './../../../../resources/pictures/treatments/underbensmassage.jpg';
import oronakupunktur from './../../../../resources/pictures/treatments/ronakupunkture.jpg';
import facelift from './../../../../resources/pictures/treatments/naturlig-facelift.jpg';
import fotvard from './../../../../resources/pictures/treatments/medicinsk-fotvrd.jpg';
import rygg from './../../../../resources/pictures/treatments/massage-rygg.jpg';
import kakbild from './../../../../resources/pictures/treatments/kakledsterapi.webp';
import ansiktszonterapi from './../../../../resources/pictures/treatments/ansiktszonterapi.jpg';
import healing from './../../../../resources/pictures/treatments/healing.jpg';
import cnm from './../../../../resources/pictures/treatments/cnm.webp';

export default function TreatmentsGrid(props) {
	return (
		<Container fluid className='text-center' style={ContainerStyle}>
			<CardColumns style={GridStyle}>
				<TreatmentsCard
					label='Taktil massage'
					description='Helkroppsbehandling med taktil massage. En lätt beröringsmassage som aktiverar kroppens system för lugn och&nbsp;ro.'
					img={taktil}
					price='75 minuter 920 kr/50 minuter 650 kr'
					alt='Taktil massage'
				/>
				<TreatmentsCard
					label='Klassisk massage'
					description='Massage har en avslappnande effekt på kroppen, stärker immunförsvaret och motverkar stress. Kan fås som hel- eller&nbsp;halvkroppsmassage.'
					img={rygg}
					alt='Klassisk massage'
					price='50 minuter 650 kr/30 minuter 430 kr'
				/>
				<TreatmentsCard
					label='Naturlig facelift'
					description='I denna behandling masserar jag ansikte, nacke, skuldror och huvud. En naturlig facelift som löser upp spänningar och gör att du känner dig&nbsp;piggare.'
					img={facelift}
					price='45 minuter 650 kr'
					alt='Naturlig facelift'
				/>
				<TreatmentsCard
					label='Öronakupunktur'
					description='Hela kroppepn avspeglar sig i örat, på samma sätt som på fötterna. Med öronakupunktur behandlas smärttillstånd, akuta och kroniska tillstånd. Det lämpar sig även för avvänjning av olika slag på grund av den nära kopplingen till centrala&nbsp;nervsystemet.'
					img={oronakupunktur}
					price='30 minuter 430 kr'
					alt='Öronakupunktur'
				/>
				<TreatmentsCard
					label='Ansiktszonterapi'
					description='Ansiktet avspeglar hur vi mår i resten av kroppen. Genom att behandla ansiktet med tryck på olika zoner och punkter förbättras dels cirkulationen lokalt i ansiktet, dels aktiveras motsvarande organ i andra delar av&nbsp;kroppen.'
					img={ansiktszonterapi}
					price='45 minuter 650 kr'
					alt='Ansiktszonterapi'
				/>
				<TreatmentsCard
					label='Zonterapi'
					description='Du kan välja mellan Fotzonterapi eller Ansiktszonterapi. På foten och i ansiktet finns punkter och zoner som motsvarar olika organ och delar på din kropp. När dessa zoner behandlas skickas signaler tillbaka till motsvarande ställe, och kroppens egen självläkning triggas&nbsp;igång.'
					img={zonterapi}
					price='45 minuter 650 kr'
					alt='Zonterapi'
				/>
				<TreatmentsCard
					label='Fot- & underbensmassage'
					description='Avslappnande massage som mjukar upp och ökar blodcirkulationen i fötter och underben. En energigivande behandling som ökar ditt välbefinnande. Kan vara bra för trötta fötter och onda&nbsp;knän.'
					img={underben}
					price='30 minuter 430 kr'
					alt='Fot- & underbensmassage'
				/>
				<TreatmentsCard
					label='Medicinsk fotvård'
					description='Behandlingen börjar med ett värmande fotbad och därefter klipper & slipar jag dina naglar. Jag filar dina fötter och ser om du har förhårdnader, liktornar mm som ska tas bort. Behandlingen avslutas med en skön&nbsp;fotmassage.'
					img={fotvard}
					price='45 minuter 650 kr'
					alt='Medicinsk fotvård'
				/>
				<TreatmentsCard
					label='Käkledsterapi'
					description='Biter du ihop och gnisslar tänder? Har du symtom som huvudvärk, käksmärta, tinnitus, knäppning i käkleden, ont i nacken eller har svårt att öppna munnen. Då kan käkledsbehandling vara något för dig. Här går jag igenom alla käkmuskler, även inne i&nbsp;munnen.'
					img={kakbild}
					price='45 minuter 650 kr'
					alt='Käkledsterapi'
				/>
				<TreatmentsCard
					label='Healing'
					description='Energin behöver flöda fritt genom våra kroppar för att vi ska må bra. Vi kan t.ex. få blockeringar i energin pga stress, sorg, för mycket jobb och för lite återhämtning. Jag kan stötta dig i att återfå ditt sköna energiflöde och din glädje genom att rensa bort blockeringar och fylla på ny&nbsp;energi.'
					img={healing}
					price='45 minuter 650 kr'
					alt='Käkledsterapi'
				/>
				<TreatmentsCard
					label='Cirkulation- och nervbanemassage (CNM)'
					description='CNM är en djupgående gnuggmassage som öppnar upp cirkulationsspärrar och ger genomblödning till alla vävnader. Vävnad som får dålig syresättning reagerar med smärta, inlagring av slaggprodukter och nedsatt funktion. Behandlingen påverkar primärt blodkärl, lymfa och nervvävnad. Kan fås som helkropps- (120 min) eller halvkroppsmassage (75 min).'
					img={cnm}
					price='120 minuter 1400 kr/75 minuter 920 kr'
					alt='Cirkulation- och nervbanemassage (CNM)'
				/>
			</CardColumns>
		</Container>
	);
}

const ContainerStyle = {
	display: 'flex',
	justifyContent: 'center',
	alignItems: 'center',
};

const GridStyle = {
	display: 'flex',
	flexGrow: 1,
	flexWrap: 'wrap',
	justifyContent: 'center',
	columnGap: '30px',
	maxWidth: '1800px',
};
