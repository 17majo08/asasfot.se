import React from 'react';

import TreatmentsGrid from './TreatmentsGrid';

export default function Treatments() {
	return (
		<div style={ContainerStyle}>
			<h1 className='Capitalized' style={HeaderStyle}>
				Behandlingar
			</h1>
			<TreatmentsGrid></TreatmentsGrid>
		</div>
	);
}

const ContainerStyle = {
	marginTop: '40px',
};

const HeaderStyle = {
	fontFamily: 'Benne',
	fontSize: '32px',
	paddingBottom: '30px',
};
