import React, { Component } from 'react';

import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';

import PropTypes from 'prop-types';

export default class TreatmentsCard extends Component {
	render() {
		return (
			<div>
				<Col className='container-fluid mt-4'>
					<Card style={CardStyle}>
						<Card.Img
							variant='top'
							src={this.props.img}
							alt={this.props.alt}
							style={{
								borderBottomLeftRadius: '20px',
								borderBottomRightRadius: '20px',
							}}
						/>
						<Card.Body>
							<h2 style={{ fontSize: '20px' }}>{this.props.label}</h2>
							<Card.Text>{this.props.description}</Card.Text>
							<Card.Subtitle className='mb-2 text-muted'>
								{this.props.price}
							</Card.Subtitle>
						</Card.Body>
					</Card>
				</Col>
			</div>
		);
	}
}

const CardStyle = {
	//width: '22rem',
	//minWidth: '20rem',
	//maxWidth: '90vw',
	width: '90vw',
	maxWidth: '22rem',
	borderRadius: '20px',
	overflow: 'hidden',
	boxShadow: '1px 1px 30px 0 rgba(100, 100, 100, 0.7)',
};

TreatmentsCard.propTypes = {
	img: PropTypes.string.isRequired,
	alt: PropTypes.string.isRequired,
	label: PropTypes.string.isRequired,
	description: PropTypes.string.isRequired,
	price: PropTypes.string.isRequired,
};
