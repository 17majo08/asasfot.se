import React from 'react';
import { Media } from 'reactstrap';
import Fade from 'react-reveal/Fade';

import gras from './../../../../resources/pictures/gras.jpg';
import house from './../../../../resources/pictures/house.jpg';

import { GiYinYang } from 'react-icons/gi';

export default function Welcome() {
	return (
		<div style={WelcomeStyle}>
			<h1 className='Capitalized' style={HeaderStyle}>
				Välkommen till Åsas Fot & Hälsa
			</h1>
			<Fade left duration={1500}>
				<Media style={MediaStyle}>
					<Media left>
						<Media object src={house} alt='' style={MediaPictureStyle} />
					</Media>
					<Media
						body
						style={{
							...MediaTextStyle,
							maxWidth: '570px',
							marginLeft: '0px',
						}}>
						<Media heading style={MediaTextHeaderStyle}>
							Välkommen hit!
						</Media>
						<p>
							Jag har lite olika behandlingar på min meny, allt från fotvård
							till zonterapi och massage. Du kan läsa mer om varje behandling
							längre ner på&nbsp;sidan. Varför inte testa{' '}
							<strong>HEALING </strong> och få ny härlig energi, samtidigt som
							det läker obalanser i din&nbsp;kropp. Välkommen att boka&nbsp;tid.
						</p>
					</Media>
				</Media>
			</Fade>

			<Fade right delay={500} duration={1500}>
				<Media style={MediaStyle}>
					{/*<Media right className='d-block d-md-none'>
						<Media
							object
							src={gras}
							alt='Generic placeholder image'
							style={MediaPictureStyle}
						/>
					</Media>*/}
					<Media
						body
						style={{
							...MediaTextStyle,
							maxWidth: '570px',
							marginRight: '0px',
						}}>
						<Media heading style={MediaTextHeaderStyle}>
							Må-Bra-Tips
						</Media>
						<h6 className='Capitalized' style={{ paddingTop: '10px' }}>
							<GiYinYang /> <strong>Timjante</strong>
						</h6>
						<p>
							Drick timjante varje dag, gärna med honung och citron. Timjan
							hjälper kroppen att mota bort virus och är en
							kraftfull&nbsp;vinterboost.
						</p>
						<h6 className='Capitalized' style={{ paddingTop: '10px' }}>
							<GiYinYang /> <strong>Öronmassage</strong>
						</h6>
						<p>
							Eftersom hela kroppen avspeglar sig i örat är öronmassage ett
							enkelt sätt att ge sig själv en&nbsp;helkroppsmassage.
						</p>
						<h6 className='Capitalized'>
							<GiYinYang /> <strong>Skratta varje dag</strong>
						</h6>
						<p>
							Möt livet med ett leende och lev längre! Om du skrattar stärks
							ditt immunförsvar och du får ny härlig&nbsp;energi.
						</p>
						<h6 className='Capitalized'>
							<GiYinYang /> <strong>Jorda dig</strong>
						</h6>
						<p>
							Stå barfota i gräset en stund varje dag och få läkande kraft från
							Moder Jord. Ger ökad energi, minskar stress och inflammationer,
							påskyndar läkning&nbsp;mm
						</p>
					</Media>
					{/*<Media right className='d-none d-md-block'>*/}
					<Media className='m-auto' right>
						<Media object src={gras} alt='' style={MediaPictureStyle} />
					</Media>
				</Media>
			</Fade>
		</div>
	);
}

const MediaStyle = {
	maxWidth: '1200px',
	width: '100%',
	paddingBottom: '30px',
	display: 'flex',
	flexGrow: 1,
	flexDirection: 'row',
	flexWrap: 'wrap',
	justifyContent: 'center',
	alignItems: 'stretch',
	alignContent: 'center',
	gap: '25px',
};

const MediaPictureStyle = {
	maxWidth: '90%',
	borderRadius: '25% 5%',
	boxShadow: '1px 1px 30px 0 rgba(0, 0, 0, 0.8)',
	marginBottom: '10px',
	justifySelf: 'center',
};

const MediaTextStyle = {
	display: 'flex',
	justifyContent: 'center',
	flexDirection: 'column',
	minWidth: '300px',
};

const MediaTextHeaderStyle = {
	paddingTop: '10px',
	fontFamily: 'Benne',
	fontWeight: 700,
};

const WelcomeStyle = {
	paddingLeft: '24px',
	paddingRight: '24px',
	paddingTop: '20px',
};

const HeaderStyle = {
	fontFamily: 'Benne',
	fontSize: '32px',
	padding: '10px 0px 40px 0px',
};
