import React from 'react';
import Image from 'react-bootstrap/Image';
import Fade from 'react-reveal/Fade';
import Welcome from './sections/welcome/Welcome';
import Treatments from './sections/treatments/Treatments';

import ThumbnailImage from '../../resources/pictures/banner-wide.jpg';

export default function Home() {
	return (
		<div style={ContainerStyle}>
			<Fade duration={5000}>
				<Image src={ThumbnailImage} thumbnail style={ThumbnailStyle} />
			</Fade>
			<Welcome />
			<Treatments />
		</div>
	);
}

const ContainerStyle = {
	display: 'flex',
	flexDirection: 'column',
	alignItems: 'center',
	justifyContent: 'center',
};

const ThumbnailStyle = {
	minWidth: '100%',
	minHeight: '250px',
	maxHeight: '500px',
	objectFit: 'cover',
	OObjectFit: 'cover',
	objectPosition: '20% 80%',
};
