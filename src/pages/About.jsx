import React from 'react';

import Image from 'react-bootstrap/Image';

import Pic from '../resources/pictures/img-01.jpg';

export default function About() {
	return (
		<div style={AboutStyle}>
			<div style={SmallScreenContainer}>
				<h1 style={HeaderStyle} className='d-block d-xl-none Capitalized'>
					Om mig
				</h1>
				<div style={ImageContainer}>
					<div style={ImageBackground}>
						<Image src={Pic} alt='aboutpicture' rounded style={ImageStyle} />
					</div>
				</div>
			</div>
			<div style={InnerTextStyle}>
				<h1 style={HeaderStyle} className='d-none d-xl-block Capitalized'>
					Om mig
				</h1>
				<p>
					Mitt företag startade jag i november 2014. Jag har alltid varit
					intresserad och fascinerad av zonterapi och dess kraft att påverka
					människans välmående och självläkning. Genom att tyda och behandla
					obalanser i kroppen via zoner - dvs punkter och områden - på fötterna
					och i ansiktet ser jag att många av mina kunder mår bättre. Kroppen
					har punkter och zoner även på öronen vilket jag också använder mig av.
					Genom öronakupuntur kan jag behandla samma zon i örat med en nål, med
					den zon jag jobbar med på foten vilket kan öka effekten och förstärker
					behandlingen. Jag använder även örat vid ”felsökning” eftersom man där
					kan se tecken på både akuta och äldre skador och&nbsp;obalanser.
				</p>
				<p>
					Efter 15 år med bl.a. support och test inom butiksdata bytte jag
					karriär och utbildade mig till medicinsk fotterapeut på Axelsons i
					Stockholm. Mitt mål var att kombinera fotvården med zonterapi, så
					efter ett år med fotvård startade jag min utbildning till zonterapeut,
					och ytterligare ett år senare i oktober 2016 fick jag mitt
					efterlängtade diplom i zonterapi. Jag har lärt mig massor om kroppen
					och hur den fungerar, och vad som händer när den är i obalans och allt
					inte fungerar som det ska. Under zonterapiutbildningen fick vi även en
					inblick i TKM – traditionell kinesisk medicin – där vi bl.a. gick
					igenom meridianer - kroppens energisystem. Längs meridianerna finns
					akupunkturpunkter som man också kan behandla via tryck, och det bästa
					av allt är att du kan fortsätta behandla själv hemma på
					dessa&nbsp;punkter.
				</p>
			</div>
		</div>
	);
}

const SmallScreenContainer = {
	display: 'flex',
	flexDirection: 'column',
};

const AboutStyle = {
	paddingLeft: '20px',
	paddingRight: '20px',
	paddingTop: '40px',
	paddingBottom: '10px',
	display: 'flex',
	flexGrow: 1,
	flexWrap: 'wrap',
	justifyContent: 'center',
	textAlign: 'center',
	width: '100%',
	columnGap: '40px',
};

const HeaderStyle = {
	fontFamily: 'Benne',
	fontSize: '30px',
};

const InnerTextStyle = {
	maxWidth: '700px',
	marginTop: '20px',
};

const ImageStyle = {
	width: '100%',
	height: 'auto',
	objectFit: 'cover',
	maxHeight: '100vh',
};

const ImageContainer = {};

const ImageBackground = {};
